# For Golang build
FROM golang:1.13.1-stretch AS builder
WORKDIR /app
COPY ./ /app/

RUN GOOS=linux go build -o fa-worm main.go
FROM alpine:latest
ENV TZ=Asia/Taipei
RUN apk --no-cache add ca-certificates tzdata
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
COPY --from=builder /app/ /